<?php
/** @noinspection ALL */


class Formatters
{
    /**
     * Provides consistent formatting for phone numbers.
     *
     * @param string $phone
     * @return string
     */
    public function FormatPhone($phone)
    {
        // Just strip anything out that isn't a number using REGEX, then format with the return
        $cleaned_phone = preg_replace("/[^0-9]/", "", $phone);
        return '(' . substr($cleaned_phone, 0, 3) . ') ' . substr($cleaned_phone, 3, 3) . '-' . substr($cleaned_phone, 6);
    }

    /**
     * Returns a singular or plural string, based on the quantity.
     *
     * @param string $singular_string
     * @param int|float $quantity
     * @return string
     */
    public function Pluralize($singular_string, $quantity)
    {
        if ($quantity != 1){
            // Pluralize
            if (substr($singular_string, -1) == 'y'){
                // string-y+ies
                return str_replace("y", "ies", $singular_string);
            }
            // string+s
            return $singular_string . 's';
        }
        return $singular_string;
    }
}
